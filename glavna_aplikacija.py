__author__ = 'Gregor, Matic'
from tkinter import *
from drzave_starost import imena

class Starost():

    def __init__(self, master):

        # Najprej starost žensk nato starost moških
        self.imena, self.starosti = imena()

        # Izbor jezika

        self.jezik = StringVar()
        self.jezik.set("Slo")
        izbjez = OptionMenu(master, self.jezik, "Slo", "Eng", "Deu")
        # izbjez.grid(row=0, column=0)
        self.jezik.trace("w", self.napisi)
        self.jezik.trace("w", self.zazeni)
        self.nstarost = StringVar(value="Starost")

        # Izbor starosti
        self.starost = IntVar()
        self.zazeni1 = IntVar(master, value=self.starost.get())
        self.napaka = StringVar(master, value="")

        fram1 = Frame(master)
        fram1.grid(row=1, column=0)
        star = Label(fram1, textvariable=self.nstarost)
        star.grid(row=0, column=0)
        # napak = Label(fram1, textvariable=self.napaka, fg="red")
        # napak.grid(row=3, column=0)

        polje1 = Entry(fram1, textvariable=self.starost, justify=CENTER, width=6)
        polje1.grid(row=1, column=0)

        poba = Label(master, textvariable=str(self.zazeni1))
        poba.grid(row=5, column=5)

        # Izbor spola
        self.spol = IntVar()
        self.spolz = StringVar(value="Ž")
        self.spolm = StringVar(value="M")
        fram2 = Frame(master)
        fram2.grid(row=1, column=1)
        spol = Label(fram2, text="Spol")
        spol.grid(row=0, column=0)

        izbsplm = Radiobutton(fram2, textvariable=self.spolm, variable=self.spol, value=1)
        izbsplm.grid(row=1, column=0)
        izbsplz = Radiobutton(fram2, textvariable=self.spolz, variable=self.spol, value=0)
        izbsplz.grid(row=1, column=1)

        # Izbor države
        self.drzava = StringVar(value="Država")
        fram3 = Frame(master)
        fram3.grid(row=1, column=2)
        drz = Label(fram3, textvariable=self.drzava, padx=5, pady=5)
        drz.grid(row=0, column=0)

        skrol = Scrollbar(fram3, orient=VERTICAL)
        skrol.grid(row=1, column=1)


        self.izbdrz = Listbox(fram3, selectmode=SINGLE)
        self.izdr = StringVar()
        for i in self.imena:
            self.izbdrz.insert(END, i.split()[0])
        self.izbdrz.grid(row=1, column=0)

        skrol.config(command=self.izbdrz.yview)

        # Potrditev
        self.zklj = StringVar(value="Potrdi")
        potrdi = Button(master, textvariable=self.zklj, command=self.zazeni)
        potrdi.grid(row=2, column=4)
        master.bind("<Return>", self.zazeni)

    def zazeni(self, kom=0, lom=0, bom=0):
        slovnap = {"Slo": "Napaka", "Eng": "Error", "Deu": "Fehler"}

        a = int(self.spol.get())
        b = int(self.izbdrz.curselection()[0])
        c = self.starosti[b][a]
        print(c)
        c = int(float(c))
        self.zazeni1.set("Po pričakovani Cinih prdvidevanji imate še {} let življenja."
                         .format(c - int(self.starost.get())))
        self.napaka.set("")
        # except:self.napaka.set(slovnap[self.jezik.get()])

    def napisi(self, asd, assf, asf):
        slovstar = {"Slo": "Starost", "Eng": "Age", "Deu": "Alter"}
        slovspolm = {"Slo": "M", "Eng": "M", "Deu": "M"}
        slovspolz = {"Slo": "Ž", "Eng": "W", "Deu": "F"}
        slovdrz = {"Slo": "Država", "Eng": "Country", "Deu": "Staat"}
        slovzk = {"Slo": "Potrdi", "Eng": "Confirm", "Deu": "Bestätigung"}
        a = self.jezik.get()

        self.nstarost.set(slovstar[a])
        self.spolm.set(slovspolm[a])
        self.spolz.set(slovspolz[a])
        self.drzava.set(slovdrz[a])
        self.zklj.set(slovzk[a])





root = Tk()

root.title("Pričakovana življenska doba")

aplikacija = Starost(root)


root.mainloop()