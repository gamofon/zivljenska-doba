__author__ = 'Matic'
import requests
import re


def imena():
    source = requests.get("https://www.cia.gov/library/publications/the-world-factbook/fields/2102.html#138",
                      auth=("user", "pass"))

    drzave = re.findall("style=\"font-weight: bold;\" >(.*)</a>", source.text)

    starost_ženske = re.findall("<strong>female:</strong> (\d*.\d*|NA) ", source.text)

    starost_moški = re.findall("<strong>male:</strong> (\d*.\d*|NA) ", source.text)
    drzave.pop(drzave.index("Curacao"))

    sl = dict()

    for i in range(len(drzave) - 1):
        sl[i] = [starost_ženske[i], starost_moški[i]]

    return drzave, sl

